# -*- coding: utf-8 -*-

import os
import sys

import urllib
import urllib2

from BeautifulSoup import BeautifulSoup

class Downloader(object):
	
	def __init__(self, url):
		self.url = url
		self.soup = BeautifulSoup(urllib2.urlopen(self.url).read())
		self.title = self.soup.title.string

		print self.url
		print self.title

	def download_cartoon(self):
		try:
			os.mkdir(self.title)
		except OSError:
			print '이미 존재하는 폴더 이름입니다.'

		cartoons = self.soup.find('div', {'id': 'post'})

		if cartoons == None:
			cartoons = self.soup.find('div', {'id': 'recent-post'})


		count = 1
		for child in cartoons.findChildren():
			if child.find('a') != None:
				print self.title + ' ' + str(count) + ' downloading..'
				self.download_pages(child.find('a')['href'])
				print 'success'
				count += 1
			

	def download_pages(self, url):
		cartoon_soup = BeautifulSoup(urllib2.urlopen(url).read())

		try:
			os.mkdir(self.title + '/' + cartoon_soup.title.string)
		except OSError:
			print '이미 존재하는 폴더 이름입니다.'

		count = 0
		for article in cartoon_soup('p')[0].contents:
			print article['href']
			urllib.urlretrieve(article['href'], self.title + '/' + cartoon_soup.title.string + '/' + str(count) + '.jpg')
			count += 1